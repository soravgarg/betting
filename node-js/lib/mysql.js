"use strict";

/*
 * Purpose: For MySQL database connection 
 * Author : Sorav Garg (soravgarg123@gmail.com)
*/

const mysql = require('mysql');

/* Require Enviornment File  */
require('dotenv').config();

const connection   = mysql.createConnection({
                            host: process.env.MYSQL_DB_HOST,
                            user: process.env.MYSQL_DB_USER,
                            password: process.env.MYSQL_DB_PASSWORD,
                            database: process.env.MYSQL_DB_NAME,
                            multipleStatements: true,
                            charset:"utf8mb4_unicode_ci"
                        });

connection.connect(function(err) {
    if (err) throw err;
    console.log("MySQL Database Connected");
});

module.exports = connection;

/* End of file mysql.js */
/* Location: ./lib/mysql.js */