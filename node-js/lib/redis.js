"use strict";

/*
 * Purpose: For redis (caching) connection 
 * Author : Sorav Garg (soravgarg123@gmail.com)
*/

const redisClustr = require('redis-clustr'),
	  redis = require('redis');


/* Require Enviornment File  */
require('dotenv').config();
if(process.env.APP_ENV == 'local'){
    var connection =  redis.createClient({
                        host: process.env.REDIS_HOST,
                        port: process.env.REDIS_PORT
                    });
}else{
    var connection = new redisClustr({
        servers: [
            {
                host: process.env.REDIS_HOST,
                port: process.env.REDIS_PORT
            }
        ],
        createClient: function (port, host) {
            // this is the default behaviour
            return redis.createClient(port, host);
        }
    });
}

/* connect to redis */
connection.on("connect", function () {
  console.log("Redis Cache connected");
});

connection.on("error", function(error) {
	console.error('Redis Cache error',error);
});

module.exports = connection;

/* End of file redis.js */
/* Location: ./lib/redis.js */