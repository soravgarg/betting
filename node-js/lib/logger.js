"use strict";

/*
 * Purpose: For logging API & Crons Requests
 * Author : Sorav Garg (soravgarg123@gmail.com)
*/

const opts = {
    errorEventName:'error',
        logDirectory:'./logs', // NOTE: folder must exist and be writable...
        fileNamePattern:'betting-<DATE>.log',
        dateFormat:'YYYY.MM.DD'
};

const log = require('simple-node-logger').createRollingFileLogger(opts);

module.exports = log;

/* End of file logger.js */
/* Location: ./lib/logger.js */