"use strict";

/*
 * Purpose: Database query builder
 * Author : Sorav Garg (soravgarg123@gmail.com)
*/

const QueryBuilder = require('node-querybuilder');
const qb = new QueryBuilder({
						    host: process.env.MYSQL_DB_HOST,
						    user: process.env.MYSQL_DB_USER,
						    password: process.env.MYSQL_DB_PASSWORD,
						    database: process.env.MYSQL_DB_NAME,
						    multipleStatements: true,
						    charset:"utf8mb4_unicode_ci"
						}, 'mysql', 'single');



class Model {

	constructor() 
	{
		this.limit = 10;
		this.order_type = 'DESC';
	}

	/**
	 * To insert data into table
	 * @param {string} table
	 * @param {object} dataObj
	*/
    insertData(callBack,table,dataObj)
    {
    	qb.insert(table, dataObj, (err, res, conn) => {
		    if (err){
                return callBack(err, res);
		    }else{
		    	return callBack(err, res);
		    }
		    conn.release();
		});
    }

    /**
	 * To insert bulk data into table
	 * @param {string} table
	 * @param {object} dataObj
	*/
    insertBulkData(callBack,table,dataObj)
    {
    	qb.insert_batch(table, dataObj, (err, res, conn) => {
		    if (err){
                return callBack(err, res);
		    }else{
		    	return callBack(err, res);
		    }
		    conn.release();
		});
    }

    /**
	 * To update data into table
	 * @param {string} table
	 * @param {object} dataObj
	 * @param {object} whereObj
	*/
    updateData(callBack,table,dataObj,whereObj)
    {
    	qb.update(table, dataObj, whereObj , (err, res, conn) => {
		    if (err){
                return callBack(err, res);
		    }else{
		    	return callBack(err, res);
		    }
		    conn.release();
		});
    }

    /**
	 * To update bulk data into table
	 * @param {string} table
	 * @param {object} dataObj
	 * @param {string} key
	 * @param {object} whereObj
	*/
    updateBulkData(callBack,table,dataObj,key,whereObj)
    {
    	qb.update_batch(table, dataObj,key,whereObj, (err, res, conn) => {
		    if (err){
                return callBack(err, res);
		    }else{
		    	return callBack(err, res);
		    }
		    conn.release();
		});
    }

    /**
	 * To delete data from table
	 * @param {string} table
	 * @param {object} whereObj
	*/
    deleteData(callBack,table,whereObj)
    {
    	qb.delete(table, whereObj , (err, res, conn) => {
		    if (err){
                return callBack(err, res);
		    }else{
		    	return callBack(err, res);
		    }
		    conn.release();
		});
    }

    /**
	 * To fire custom query
	 * @param {string} query
	*/
    customQuery(callBack,query)
    {
    	qb.query(query , (err, res, conn) => {
		    if (err){
                return callBack(err, res);
		    }else{
		    	return callBack(err, res);
		    }
		    conn.release();
		});
    }

    /**
	 * To get whole table data
	 * @param {string} table
	 * @param {string} orderField
	 * @param {string} orderType
	 * @param {string} fields
	 * @param {integer} limit
	 * @param {integer} offset
	 * @param {string} groupBy
	*/
    getAll(callBack,table,orderField,orderType,fields,limit,offset,groupBy)
    {
    	if(fields){
    		qb.select(fields);
    	}else{
    		qb.select('*');
    	}
    	if(orderField && orderType){
    		qb.order_by(orderField, orderType);
    	}
    	if(limit && limit > 0){
    		qb.limit(limit);
    	}
    	if(offset && offset > 0){
    		qb.offset(offset);
    	}
    	if(groupBy){
    		qb.group_by(groupBy);
    	}
    	qb.get(table, (err,res, conn) => {
    		if (err){
                return callBack(err, res);
		    }else{
		    	return callBack(err, res);
		    }
		    conn.release();
	    });
    }

    /**
	 * To get conditional data
	 * @param {string} table
	 * @param {object} whereObj
	 * @param {string} orderField
	 * @param {string} orderType
	 * @param {string} fields
	 * @param {integer} limit
	 * @param {integer} offset
	 * @param {string} groupBy
	*/
    getAllWhere(callBack,table,whereObj,orderField,orderType,fields,limit,offset,groupBy)
    {
    	if(fields){
    		qb.select(fields);
    	}else{
    		qb.select('*');
    	}
    	if(whereObj){
    		qb.where(whereObj);
    	}
    	if(orderField && orderType){
    		qb.order_by(orderField, orderType);
    	}
    	if(limit && limit > 0){
    		qb.limit(limit);
    	}
    	if(offset && offset > 0){
    		qb.offset(offset);
    	}
    	if(groupBy){
    		qb.group_by(groupBy);
    	}
    	qb.get(table, (err,res, conn) => {
    		if (err){
                return callBack(err, res);
		    }else{
		    	return callBack(err, res);
		    }
		    conn.release();
	    });
    }

    /**
	 * To get count of total rows
	 * @param {string} table
	 * @param {object} whereObj
	 * @param {string} groupBy
	*/
    getCount(callBack,table,whereObj,groupBy)
    {
    	if(whereObj){
    		qb.where(whereObj);
    	}
    	if(groupBy){
    		qb.group_by(groupBy);
    	}
    	qb.count(table, (err,res, conn) => {
    		if (err){
                return callBack(err, res);
		    }else{
		    	return callBack(err, res);
		    }
		    conn.release();
	    });
    }

    /**
	 * To get min, max, average, sum of results
	 * @param {string} table
	 * @param {string} fieldName
	 * @param {string} type
	 * @param {object} whereObj
	 * @param {string} groupBy
	*/
    getMinMaxAvgsum(callBack,table,fieldName,type,whereObj,groupBy)
    {
    	switch(type) {
		    case "MIN":
		        qb.select_min(fieldName, 'result')
		        break;
		    case "MAX":
		        qb.select_max(fieldName, 'result')
		        break;
		    case "AVG":
		        qb.select_avg(fieldName, 'result')
		        break;
		    case "SUM":
		        qb.select_sum(fieldName, 'result')
		        break;
		    default:
		}
    	if(whereObj){
    		qb.where(whereObj);
    	}
    	if(groupBy){
    		qb.group_by(groupBy);
    	}
    	qb.get(table, (err,res, conn) => {
    		if (err){
                return callBack(err, res);
		    }else{
		    	return callBack(err, res);
		    }
		    conn.release();
	    });
    }

}

module.exports = new Model();

/* End of file model.js */
/* Location: ./lib/model.js */