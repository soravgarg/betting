"use strict";

/* Purpose: Running node.js and initialize. 
 * Author : Sorav Garg (soravgarg123@gmail.com)
*/

const app   = require('express')(),
	  express = require('express'),
    cors    = require('cors'),
    axios   = require('axios'),
    compression = require('compression'),
	  server  = require('http').createServer(app),
	  expressValidator = require('express-validator'),
	  bodyParser = require('body-parser'),
    log = require('./lib/logger.js'),
    dateTime = require('./lib/datetime.js'),
    helper = require('./lib/helper.js'),
    cron = require('node-cron');

/* Require Enviornment File  */
require('dotenv').config();

/* Require Preety Error  */
require('pretty-error').start();

/* To set port */
app.set('port', process.env.PORT || 3000);

/* Compress all HTTP responses */
app.use(compression());

/* To Listen Port */
// server.listen(app.get('port'), function () {
//   console.log(`Express server listening on port ${app.get('port')}`);
// });

var nodeServer = server.listen(300 + process.env.NODE_APP_INSTANCE, () => {
    console.log("Server running on port: ", nodeServer.address().port);
});

/* Cross Origin */
app.use(cors())

/* BodyParser Middleware */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({limit: '500mb', extended: true}));

/* Handle Invalid JSON */
app.use((err, req, res, next) => {
    if(err.status === 400){
      var ErrorObj = {ResponseCode: 400, Message:'Invalid JSON request.'}
      log.warn('Error Log -> ', ErrorObj);
      return res.status(400).json(ErrorObj);
    }
    return next(err); // if it's not a 400, let the default error handling do it. 
})

server.timeout = 1200000;
global.InPlayMarkets = [];
global.UpcomingMarkets = [];

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

/* Crons & API Routings */
app.use('/crons/', require('./crons/cricket/routes/index'));
app.use('/api/', require('./api/common/routes/index'));
app.use('/api/', require('./api/front/cricket/routes/index'));

/* Redirect On Website */
app.get('/', function(req, res) {
	return res.redirect(process.env.APP_BASE_URL)
})

/* Run Cricket Market Bettings (In Play) */
cron.schedule('* * * * * *', () => { // every 1 second
  let SourceApiUrl = process.env.APP_BASE_URL + '/crons/cricket/market/booking-list/';
  if(process.env.SOURCE_API_NAME === 'AZURE'){
   SourceApiUrl += 'azure/inplay';
  }else if(process.env.SOURCE_API_NAME === 'SWAGGER'){
   SourceApiUrl += 'swagger/inplay';
  }
  helper.callSportsDataApi(SourceApiUrl,function(data,err){
    if(err){
      console.log('err inplay',err)
    }
  })
});

/* Run Cricket Market Bettings (Upcoming) */
cron.schedule('*/10 * * * * *', () => { // every 10 seconds
  let SourceApiUrl = process.env.APP_BASE_URL + '/crons/cricket/market/booking-list/';
  if(process.env.SOURCE_API_NAME === 'AZURE'){
   SourceApiUrl += 'azure/upcoming';
  }else if(process.env.SOURCE_API_NAME === 'SWAGGER'){
   SourceApiUrl += 'swagger/upcoming';
  }
  helper.callSportsDataApi(SourceApiUrl,function(data,err){
    if(err){
      console.log('err upcoming',err)
    }
  })
});

/* Handle Invalid Url */
app.all('*', (req, res, next) => {
  var ErrorObj = {ResponseCode: 404, Message: `Can't find ${req.originalUrl} on this server!`}
  log.warn('Error Log -> ', ErrorObj);
  res.status(404).json(ErrorObj);
}); 

module.exports = { app };

/* End of file index.js */
/* Location: ./index.js */
