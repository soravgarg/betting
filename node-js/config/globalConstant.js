"use strict";

/*
 * Purpose: To define all constants
 * Author : Sorav Garg (soravgarg123@gmail.com)
*/

/* Require Enviornment File  */
require('dotenv').config();

var appConstant = function () {

	/* Site Details */
	this.SITE_NAME = "Betting";
	 
	/* Base Url  */
	this.BASE_URL  = process.env.APP_BASE_URL + '/betting/';

	/* Crons & Api Constants */
	this.CRICKET_MARKETS  = '/cricket/markets';
	this.CRICKET_MARKET_BOOKING_LIST  = '/cricket/market/booking-list/';
	return this;
}

module.exports = new appConstant();