"use strict";

/* Purpose : Crons For Cricket
   Package : Cricket 
   Developed By  : Sorav Garg (soravgarg123@gmail.com) 
*/

const async = require("async"),
      lodash = require("lodash"),
      axios = require("axios"),
      globalConstant = require("../../../config/globalConstant"),
      helper = require("../../../lib/helper"),
      redis    = require('../../../lib/redis');

const { promisify } = require("util");
const getLrange = promisify(redis.lrange).bind(redis);
const delLrem   = promisify(redis.lrem).bind(redis);
const setData   = promisify(redis.set).bind(redis);

let cricketController = {azure_market_booking_list_in_play,azure_market_booking_list_upcoming,swagger_market_booking_list_in_play,swagger_market_booking_list_upcoming};  
  /**
   * For Cricket Market Booking List (Azure) In Play
  */
  async function azure_market_booking_list_in_play(req, res) {
    // let Markets = await getLrange(globalConstant.CRICKET_MARKETS, 0, -1);
    let Markets = lodash.uniq(global.InPlayMarkets);
    if(Markets.length === 0){
      return res.status(200).json({
              ResponseCode: 200,
              Message:'Markets not found.'
          });
    }

    /* Divide All Markets Into 15 Sub Market */
    let GroupedMarkets = lodash.chunk(Markets,15);
    let MarketBookingData = {};

    /* Reading in parallel */
    await Promise.all(GroupedMarkets.map(async (MarketsArr) => {
      var OpenMarkets = [];
      try {
        var MarketsData = await axios.get(process.env.AZURE_BETFAIR_BETTING_API + MarketsArr.join());
      } catch (error) {
        console.log('axios api error',error)
        return;
      }
      if(!lodash.isEmpty(MarketsData.data)){
        var data = MarketsData.data;
        for (let index = 0; index < data.length; index++) {
          if(data[index].status === 'CLOSED'){
            // await delLrem(globalConstant.CRICKET_MARKETS, 0, data[index].marketId);
            global.InPlayMarkets.splice(global.InPlayMarkets.indexOf(data[index].marketId), 1);
            continue;
          }
          if(!data[index].inplay){ // true or false
            global.InPlayMarkets.splice(global.InPlayMarkets.indexOf(data[index].marketId), 1);
            if(!global.UpcomingMarkets.includes(data[index].marketId)){
              global.UpcomingMarkets.push(data[index].marketId);
            }
          }
          OpenMarkets.push(data[index].marketId);
          MarketBookingData[data[index].marketId] = data[index];
        }

        /* Remove Empty Data Markets */
        for (let subIndex = 0; subIndex < MarketsArr.length; subIndex++) {
          if(!OpenMarkets.includes(MarketsArr[subIndex])){
            // await delLrem(globalConstant.CRICKET_MARKETS, 0, MarketsArr[subIndex]);
            global.InPlayMarkets.splice(global.InPlayMarkets.indexOf(MarketsArr[subIndex]), 1);
          }
        }
      }
    }));

    /* Save Market Bookings Data */
    let responseData = await setData(globalConstant.CRICKET_MARKET_BOOKING_LIST,JSON.stringify(MarketBookingData));
    return res.status(200).json(responseData);
  }

  /**
   * For Cricket Market Booking List (Azure) Upcoming
  */
  async function azure_market_booking_list_upcoming(req, res) {
    // let Markets = await getLrange(globalConstant.CRICKET_MARKETS, 0, -1);
    let Markets = lodash.uniq(global.UpcomingMarkets);
    if(Markets.length === 0){
      return res.status(200).json({
              ResponseCode: 200,
              Message:'Markets not found.'
          });
    }

    /* Divide All Markets Into 15 Sub Market */
    let GroupedMarkets = lodash.chunk(Markets,15);
    let MarketBookingData = {};

    /* Reading in parallel */
    await Promise.all(GroupedMarkets.map(async (MarketsArr) => {
      var OpenMarkets = [];
      try {
        var MarketsData = await axios.get(process.env.AZURE_BETFAIR_BETTING_API + MarketsArr.join());
      } catch (error) {
        console.log('axios api error',error)
        return;
      }
      if(!lodash.isEmpty(MarketsData.data)){
        var data = MarketsData.data;
        for (let index = 0; index < data.length; index++) {
          if(data[index].status === 'CLOSED'){
            // await delLrem(globalConstant.CRICKET_MARKETS, 0, data[index].marketId);
            global.UpcomingMarkets.splice(global.UpcomingMarkets.indexOf(data[index].marketId), 1);
            continue;
          }
          if(data[index].inplay){ // true or false
            global.UpcomingMarkets.splice(global.UpcomingMarkets.indexOf(data[index].marketId), 1);
            if(!global.InPlayMarkets.includes(data[index].marketId)){
              global.InPlayMarkets.push(data[index].marketId);
            }
          }
          OpenMarkets.push(data[index].marketId);
          MarketBookingData[data[index].marketId] = data[index];
        }

        /* Remove Empty Data Markets */
        for (let subIndex = 0; subIndex < MarketsArr.length; subIndex++) {
          if(!OpenMarkets.includes(MarketsArr[subIndex])){
            // await delLrem(globalConstant.CRICKET_MARKETS, 0, MarketsArr[subIndex]);
            global.UpcomingMarkets.splice(global.UpcomingMarkets.indexOf(MarketsArr[subIndex]), 1);
          }
        }
      }
    }));

    /* Save Market Bookings Data */
    let responseData = await setData(globalConstant.CRICKET_MARKET_BOOKING_LIST,JSON.stringify(MarketBookingData));
    return res.status(200).json(responseData);
  }

  /**
   * For Cricket Market Booking List (Swagger) In Play
  */
  async function swagger_market_booking_list_in_play(req, res) {
    let Markets = lodash.uniq(global.InPlayMarkets);
    console.log('Markets',Markets)
    if(Markets.length === 0){
      res.status(200).json({
              ResponseCode: 200,
              Message:'Markets not found.'
          });
      return;
    }

    /* Divide All Markets Into 25 Sub Market */
    let GroupedMarkets = lodash.chunk(Markets,25);
    let MarketBookingData = {};

    /* Reading in parallel */
    await Promise.all(GroupedMarkets.map(async (MarketsArr) => {
      var OpenMarkets = [];
      try {
        var MarketsData = await axios.post(process.env.SWAGGER_BETFAIR_BETTING_API,{'marketIds' : MarketsArr},{headers: {'Content-Type': 'application/json','X-App': 'oEH3gBzWO6'}});
      } catch (error) {
        console.log('axios api error',error)
        return;
      }
      if(!lodash.isEmpty(MarketsData.data.result)){
        var data = MarketsData.data.result;
        for (let index = 0; index < data.length; index++) {
          if(data[index].status === 'CLOSED'){
            // await delLrem(globalConstant.CRICKET_MARKETS, 0, data[index].marketId);
            global.InPlayMarkets.splice(global.InPlayMarkets.indexOf(data[index].marketId), 1);
            continue;
          }
          if(!data[index].inplay){ // true or false
            global.InPlayMarkets.splice(global.InPlayMarkets.indexOf(data[index].marketId), 1);
            if(!global.UpcomingMarkets.includes(data[index].marketId)){
              global.UpcomingMarkets.push(data[index].marketId);
            }
          }
          OpenMarkets.push(data[index].marketId);
          let Runners = data[index].runners;
          MarketBookingData[data[index].marketId] = data[index];
          MarketBookingData[data[index].marketId].numberOfRunners = Runners.length;
        }

        /* Remove Empty Data Markets */
        for (let subIndex = 0; subIndex < MarketsArr.length; subIndex++) {
          if(!OpenMarkets.includes(MarketsArr[subIndex])){
            // await delLrem(globalConstant.CRICKET_MARKETS, 0, MarketsArr[subIndex]);
            global.InPlayMarkets.splice(global.InPlayMarkets.indexOf(MarketsArr[subIndex]), 1);
          }
        }
      }
    }));

    /* Save Market Bookings Data */
    let responseData = await setData(globalConstant.CRICKET_MARKET_BOOKING_LIST,JSON.stringify(MarketBookingData));
    return res.status(200).json(responseData);
  }

  /**
   * For Cricket Market Booking List (Swagger) Upcoming
  */
  async function swagger_market_booking_list_upcoming(req, res) {
    let Markets = lodash.uniq(global.UpcomingMarkets);
    console.log('Markets',Markets)
    if(Markets.length === 0){
      res.status(200).json({
              ResponseCode: 200,
              Message:'Markets not found.'
          });
      return;
    }

    /* Divide All Markets Into 25 Sub Market */
    let GroupedMarkets = lodash.chunk(Markets,25);
    let MarketBookingData = {};

    /* Reading in parallel */
    await Promise.all(GroupedMarkets.map(async (MarketsArr) => {
      var OpenMarkets = [];
      try {
        var MarketsData = await axios.post(process.env.SWAGGER_BETFAIR_BETTING_API,{'marketIds' : MarketsArr},{headers: {'Content-Type': 'application/json','X-App': 'oEH3gBzWO6'}});
      } catch (error) {
        console.log('axios api error',error);
        return;
      }
      if(!lodash.isEmpty(MarketsData.data.result)){
        var data = MarketsData.data.result;
        for (let index = 0; index < data.length; index++) {
          if(data[index].status === 'CLOSED'){
            // await delLrem(globalConstant.CRICKET_MARKETS, 0, data[index].marketId);
            global.UpcomingMarkets.splice(global.UpcomingMarkets.indexOf(data[index].marketId), 1);
            continue;
          }
          if(data[index].inplay){ // true or false
            global.UpcomingMarkets.splice(global.UpcomingMarkets.indexOf(data[index].marketId), 1);
            if(!global.InPlayMarkets.includes(data[index].marketId)){
              global.InPlayMarkets.push(data[index].marketId);
            }
          }
          OpenMarkets.push(data[index].marketId);
          let Runners = data[index].runners;
          MarketBookingData[data[index].marketId] = data[index];
          MarketBookingData[data[index].marketId].numberOfRunners = Runners.length;
        }

        /* Remove Empty Data Markets */
        for (let subIndex = 0; subIndex < MarketsArr.length; subIndex++) {
          if(!OpenMarkets.includes(MarketsArr[subIndex])){
            // await delLrem(globalConstant.CRICKET_MARKETS, 0, MarketsArr[subIndex]);
            global.UpcomingMarkets.splice(global.UpcomingMarkets.indexOf(MarketsArr[subIndex]), 1);
          }
        }
      }
    }));

    /* Save Market Bookings Data */
    let responseData = await setData(globalConstant.CRICKET_MARKET_BOOKING_LIST,JSON.stringify(MarketBookingData));
    return res.status(200).json(responseData);
  }


module.exports = cricketController;
