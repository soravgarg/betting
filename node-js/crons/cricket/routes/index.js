"use strict";

/*
 * Purpose : For Cricket Crons Routing
 * Package : Router
 * Developed By  : Sorav Garg (soravgarg123@gmail.com)
*/

const express  = require('express'),
      router   = express.Router(),
      globalConstant = require('../../../config/globalConstant.js'),
      cricket        = require('../../cricket/controllers/cricketController');

      /* Cricket Routings */
      router.get(globalConstant.CRICKET_MARKET_BOOKING_LIST + 'azure/inplay',cricket.azure_market_booking_list_in_play);
      router.get(globalConstant.CRICKET_MARKET_BOOKING_LIST + 'azure/upcoming',cricket.azure_market_booking_list_upcoming);
      router.get(globalConstant.CRICKET_MARKET_BOOKING_LIST + 'swagger/inplay',cricket.swagger_market_booking_list_in_play);
      router.get(globalConstant.CRICKET_MARKET_BOOKING_LIST + 'swagger/upcoming',cricket.swagger_market_booking_list_upcoming);

module.exports = router;      