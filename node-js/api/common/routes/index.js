"use strict";

/*
 * Purpose : For Front (Web) API's Routing
 * Package : Router
 * Developed By  : Sorav Garg (soravgarg123@gmail.com)
*/

const express  = require('express'),
      router   = express.Router(),
      sports   = require('../../common/controllers/sportsController');

      /* Sports Routings */
      router.get('/sports/clear-all-cache',sports.clear_all_cache);
      router.get('/sports/clear-cache',sports.clear_cache);
      router.get('/sports/logviewer/:FileName',sports.logviewer);
module.exports = router;      