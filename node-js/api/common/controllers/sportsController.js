"use strict";

/*
 * Purpose : API For Common Sport
 * Package : Sport
 * Developed By  : Sorav Garg (soravgarg123@gmail.com)
*/

const redis  = require('../../../lib/redis'),
      fs     = require('fs');

let sportsController = {

		/**
     * For Clear All Cache
    */
    clear_all_cache: (req, res) => {
        redis.flushdb(function(err,succeeded){
            if(err) throw err;
    		res.status(200).json({
                ResponseCode: 200,
                Data: succeeded,
                Message: 'Success'
            });
       });
    },

    /**
     * For Clear Particular Cache
    */
    clear_cache: (req, res) => {
        redis.del(req.query.key, function(err,response){
            if(err) throw err;
            if (response === 1) {
              res.status(200).json({
                    ResponseCode: 200,
                    Data: response,
                    Message: "Deleted Successfully!"
                });
            } else{
                res.status(500).json({
                    ResponseCode: 500,
                    Data: response,
                    Message: "Cannot delete"
                });
            }
       });
    },

    /**
     * For logviewer
    */
    logviewer: (req, res) => {
      fs.readFile('./logs/' + req.params.FileName, 'utf8', function(err, data){ 
          let FileData = data.split("\n");
          let ResponseObj = [];
          for (var i = 0; i < FileData.length; i++) {
            let line = FileData[i].split(' -> ');
            let log  = line[0].split(/[ ,]+/);
            ResponseObj[i] = {
              'Time' : log[0],
              'Level' : log[1],
              'LogType' : log[2] + " " + log[3],
              'Data' : (!line[1]) ? "" : JSON.parse(line[1])
            }
            if(i === (FileData.length - 1)){
              res.status(200).json({
                  ResponseCode: 200,
                  Data: ResponseObj,
                  Message: 'Success'
              });
            }
          }
      });
    }
};

module.exports = sportsController;