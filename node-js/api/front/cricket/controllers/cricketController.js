"use strict";


/*
 * Purpose : API For Cricket
 * Package : Cricket
 * Developed By  : Sorav Garg (soravgarg123@gmail.com)
 */

const async = require("async"),
    lodash  = require("lodash"), 
    globalConstant = require('../../../../config/globalConstant'),
    helper = require('../../../../lib/helper'),
    redis = require('../../../../lib/redis'),
    datetime = require('../../../../lib/datetime'), {
        check,
        matches,
        validationResult,
        matchedData
    } = require('express-validator');

const { promisify } = require("util");
const getLrange = promisify(redis.lrange).bind(redis);
const setData   = promisify(redis.set).bind(redis);
const getData   = promisify(redis.get).bind(redis);

let cricketController = {validate,add_market,markets,market_booking_list};

    /**
     * For Validation
     */
    function validate(method) {
        switch (method) {
            case 'add_market': {
                return [
                    check('marketId').notEmpty().withMessage('Market Id field is required').trim()
                ]
            }
            break;
        }
    }

    /**
      * For Add Market
    */
    async function add_market(req, res) {
        let {marketId} = req.query
        console.log('marketId',marketId)
        if(marketId && /^1./.test(marketId)){
        // var multiRedis = redis.multi();
        //     multiRedis.lpush(globalConstant.CRICKET_MARKETS, marketId);
        //     multiRedis.exec(function(err, response) {
        //         if(err) throw err;
        //         return res.status(200).json('Market added successfully');
        //     });
        if(!global.UpcomingMarkets.includes(marketId)){
            global.UpcomingMarkets.push(marketId);
        }
        console.log('hiiii',global.UpcomingMarkets)
        return res.status(200).json('Market added successfully');
        }else{
            return res.status(500).json('Require valid marketId !');
        }
    }

    /**
     * For Cricket Markets
    */
    async function markets(req, res) {
        // let items = await getLrange(globalConstant.CRICKET_MARKETS, 0, -1);
        let items = global.UpcomingMarkets;
        return res.status(200).json(lodash.uniq(items));
    }

    /**
     * For Cricket Market Booking List
    */
    async function market_booking_list(req, res) {
        var marketIds  = (!req.params.marketIds) ? null : req.params.marketIds;
        console.log('marketIds',marketIds)
        var newMarkets = [];
        if(!lodash.isEmpty(marketIds)){
            // var multiRedis   = redis.multi();
            let MarketIdsArr = marketIds.split(",");
            // let Markets = await getLrange(globalConstant.CRICKET_MARKETS, 0, -1);
            let Markets = lodash.uniq(global.UpcomingMarkets);

            /* Reading in parallel */
            await Promise.all(MarketIdsArr.map(async (Market) => {
                if(!Markets.includes(Market) && /^1./.test(Market)){
                    // multiRedis.lpush(globalConstant.CRICKET_MARKETS, Market);
                    // newMarkets.push(Market);
                    global.UpcomingMarkets.push(Market);
                }
            }));

            // if(newMarkets.length > 0){
            //  multiRedis.exec(function(err, response) {if(err) throw err; })
            // }
        }

        /* Get Bookings Data */
        let marketsData = await getData(globalConstant.CRICKET_MARKET_BOOKING_LIST);
        let responseData = (marketsData) ? Object.values(JSON.parse(marketsData)) : [];
        if(marketIds){
            responseData = await helper.filterArrMultipleValues(responseData,'marketId',marketIds.split(","));
        }
        if(newMarkets.length > 0){
            for (var i = 0; i < newMarkets.length; i++) {
                responseData.push({'marketId' : newMarkets[i]});
            }
        }
        res.status(200).json(responseData);
    }

    //     /* Get Bookings Data */
    //     let marketsData = await getData(globalConstant.CRICKET_MARKET_BOOKING_LIST);
    //     let responseData = (marketsData) ? Object.values(JSON.parse(marketsData)) : [];
    //     if(marketIds){
    //         responseData = await helper.filterArrMultipleValues(responseData,'marketId',marketIds.split(","));
    //     }
    //     if(newMarkets.length > 0){
    //         for (var i = 0; i < newMarkets.length; i++) {
    //             responseData.push({'marketId' : newMarkets[i]});
    //         }
    //     }
    //     res.status(200).json(responseData);
    // }

    /**
     * For Cricket Market Booking List
    */
    // async function market_booking_list(req, res) {
    //     var marketIds    = (!req.params.marketIds) ? null : req.params.marketIds;
    //     var marketsData  = await getData(globalConstant.CRICKET_MARKET_BOOKING_LIST);
    //     var responseData = (marketsData) ? Object.values(JSON.parse(marketsData)) : [];
    //     if(marketIds){
    //         responseData = await helper.filterArrMultipleValues(responseData,'marketId',marketIds.split(","));
    //     }
    //     res.status(200).json(responseData);
    // }

module.exports = cricketController;