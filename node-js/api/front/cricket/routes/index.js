"use strict";

/*
 * Purpose : For Cricket Front (Web) API's Routing
 * Package : Router
 * Developed By  : Sorav Garg (soravgarg123@gmail.com)
*/

const express  = require('express'),
      router   = express.Router(),
      globalConstant = require('../../../../config/globalConstant'),
      cricket  = require('../../cricket/controllers/cricketController');

      /* Cricket Routings */
      router.get('/cricket/add-market',cricket.validate('add_market'),cricket.add_market);
      router.get(globalConstant.CRICKET_MARKETS,cricket.markets);
      router.get(globalConstant.CRICKET_MARKET_BOOKING_LIST + ':marketIds?',cricket.market_booking_list);

module.exports = router;      